var productsArray = [
      {
        "sys": { "id": "1" },
        "fields": {
          "title": "Leather Sofa",
          "price": 10.99,
          "image": { "fields": { "file": { "url": "images/product-1.jpg" } } }
        }
      },
      {
        "sys": { "id": "2" },
        "fields": {
          "title": "Cozy nook",
          "price": 12.99,
          "image": { "fields": { "file": { "url": "images/product-2.jpg" } } }
        }
      },
      {
        "sys": { "id": "3" },
        "fields": {
          "title": "single sofa",
          "price": 12.99,
          "image": { "fields": { "file": { "url": "images/product-3.jpg" } } }
        }
      },
      {
        "sys": { "id": "4" },
        "fields": {
          "title": "Arm chair",
          "price": 22.99,
          "image": { "fields": { "file": { "url": "images/product-4.jpg" } } }
        }
      },
      {
        "sys": { "id": "5" },
        "fields": {
          "title": "L shaped sofa",
          "price": 88.99,
          "image": { "fields": { "file": { "url": "images/product-5.jpg" } } }
        }
      },
      {
        "sys": { "id": "6" },
        "fields": {
          "title": "TV Unit",
          "price": 32.99,
          "image": { "fields": { "file": { "url": "images/product-6.jpg" } } }
        }
      },
      {
        "sys": { "id": "7" },
        "fields": {
          "title": "Show Case",
          "price": 45.99,
          "image": { "fields": { "file": { "url": "images/product-7.jpg" } } }
        }
      },
      {
        "sys": { "id": "8" },
        "fields": {
          "title": "King Sized Bed",
          "price": 33.99,
          "image": { "fields": { "file": { "url": "images/product-8.jpg" } } }
        }
      }
    ];

var productElements = [];
var productCounter = 0;
function populateProducts() {
  var htmlElements = "";
  productsArray.forEach((product) => {
    htmlElements += '<div class="card" id="' + product.sys.id + '">' +
    '<img src="'+ product.fields.image.fields.file.url + '" alt="Product Image">'+
    '<p class="card-title">' + product.fields.title +'</p>' +
    '<p class="title">$' + product.fields.price + '</p>' +
    '<button class="add-to-cart-btn" id="product-' + product.sys.id + '" onClick="updateProductCounter(event)">Add to cart</button>' +
    '</div>';
  })
  document.getElementById("productsList").innerHTML = htmlElements;
  //Populate cart even if page refreshes incase there are items
  if (localStorage !== null) {
    var productsElementsArr = JSON.parse(localStorage.getItem("productElements"));
    if(productsElementsArr.length != 0){
      productCounter = Number(localStorage.getItem("productCounter"));
      document.getElementById("cart-value").innerHTML = productCounter;
      productsElementsArr.forEach((product) => {
        productElements.push(product);
      });
      addProductToCart(productElements);
    }
  }
}

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  //document.body.style.backgroundColor = "rgba(247, 207, 170 ,0.4)";
  //document.getElementsByTagName('button').style.backgroundColor = "rgba(247, 207, 170 ,0.4)";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.body.style.backgroundColor = "white";
}

function updateProductCartCounter(productCounter){
  localStorage.setItem("productCounter", productCounter);
  document.getElementById("cart-value").innerHTML = localStorage.getItem("productCounter");
}

function updateProductCounter(e){
  //Step 1: update cart counter
  productCounter += 1;
  updateProductCartCounter(productCounter);
  var productId = e.target.id.split('-')[1].toString();
  let productInfo = productsArray.find(o => o.sys.id === productId);
  //Find and push the product to the main products array
  productElements.push({
    productId: productInfo.sys.id,
    name: productInfo.fields.title,
    image: productInfo.fields.image.fields.file.url,
    price: productInfo.fields.price
  })
  addProductToCart(productElements);
}

var totalCartValue = 0;
function addProductToCart(productsElements) {
  //step 2: update cart products in sidenav 
  localStorage.setItem("productElements", JSON.stringify(productsElements));
  var productsElementsArr = JSON.parse(localStorage.getItem("productElements"));
  var cartElements = "";
  const uniqueProducts = [...productsElementsArr.reduce( (mp, o) => {
    if (!mp.has(o.productId)) mp.set(o.productId, { ...o, quantity: 0 });
    mp.get(o.productId).quantity++;
    return mp;
  }, new Map).values()];

  uniqueProducts.forEach((product) => {
    cartElements += '<div id="cartProduct-' + product.productId + '" data-quantity=' + product.quantity + ' data-price=' + product.price +'><img src="'+ product.image + '" alt="Product Image" class="cart-item-image">'+
    '<span class="product-detail">' + product.name +'</span><br/>' +
    '<span class="product-detail">$' + product.price + '</span><br/>' +
    '<i class="fa fa-plus-circle" id="addProductQuantity-' + product.productId + '"  data-quantity=' + product.quantity + ' onclick="addProductQuantity(event)"></i> '+
    '<span class="product-detail" id="productQuantity-' + product.productId + '">' + product.quantity + '</span>' +
    ' <i class="fa fa-minus-circle" id="removeProductQuantity-' + product.productId + '" data-quantity=' + product.quantity + ' onclick="removeProductQuantity(event)"></i><br/>' +
    '<button class="remove-item-btn" id="removeProduct-' + product.productId + '" onClick="removeProduct(event)">Remove</button></div><br/>';
  })
  document.getElementById("cart-items").innerHTML = cartElements;
  totalCartValue = productsElementsArr.reduce((accumulator,item) => accumulator + item.price, 0);
  document.getElementById("total-cart-value").innerHTML = "Your Total: $" + roundValue(totalCartValue);
}

function removeProductQuantity(e){
  var productId = e.target.id.split('-')[1].toString();
  //update product cart counter
  productCounter = productCounter - 1;
  updateProductCartCounter(productCounter);
  //remove the product from main array so the quantity is updated
  productElements.splice(productElements.findIndex(a => a.productId === productId) , 1);
  addProductToCart(productElements);
}

function addProductQuantity(e) {
  var productId = e.target.id.split('-')[1].toString();
  //update the product cart counter
  productCounter = productCounter + 1;
  updateProductCartCounter(productCounter);
  //add the product to the main array so quantity is updated
  let productInfo = productsArray.find(o => o.sys.id === productId);
  productElements.push({
    productId: productInfo.sys.id,
    name: productInfo.fields.title,
    image: productInfo.fields.image.fields.file.url,
    price: productInfo.fields.price
  })
  addProductToCart(productElements);
}

function roundValue(value){
  return Math.round((value + Number.EPSILON) * 100) / 100;
}

function removeProduct(e) {
  var productId = e.target.id.split('-')[1].toString();
  var cartProductDivId = "cartProduct-" + productId;
  const cartProductDivQuerySelector = document.getElementById(cartProductDivId);
  const productQuantity = cartProductDivQuerySelector.dataset.quantity;
  totalCartValue = roundValue(totalCartValue - (cartProductDivQuerySelector.dataset.price * productQuantity));
  //remove the product div
  cartProductDivQuerySelector.remove();
  //remove product from main array of products
  productElements = productElements.filter(item => item.productId !== productId);
  //update the product cart counter
  productCounter = productCounter - productQuantity;
  updateProductCartCounter(productCounter);
  //update the total cart value
  document.getElementById("total-cart-value").innerHTML = "Your Total: $" + totalCartValue;
}

function clearCart(){
  var container = document.querySelector("#cart-items");
  while (container.firstChild) {
    container.removeChild(container.firstChild);
  }
  //remove product from main array of products
  productElements = [];
  localStorage.clear();
  //update the product cart counter
  productCounter = 0;
  updateProductCartCounter(productCounter);
  //update the total cart value
  document.getElementById("total-cart-value").innerHTML = "Your Total: $" + 0;
}